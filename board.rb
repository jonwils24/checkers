require './piece.rb'
require './nil.rb'

class Board
  
  def initialize(fill_board = true)
    @board = Array.new(8) { Array.new(8) }
    set_pieces if fill_board
  end
  
  def [](pos)
    row, col = pos
    @board[row][col]
  end
  
  def []=(pos, piece)
    row, col = pos
    @board[row][col] = piece
  end
  
  def valid_pos?(pos)
    pos.all? { |coord| coord.between?(0, 7) }
  end
  
  def empty?(pos)
    self[pos].nil?
  end
  
  def render
    @board.map do |row|
      row.map do |piece|
        piece.inspect
      end.join
    end.join("\n")
  end
  
  def no_more_colors?(color)
    pieces.none? { |piece| piece.color == color }
  end
  
  def pieces
    @board.flatten.compact
  end
  
  def dup
    dup_board = Board.new(false)
    pieces.each do |piece|
      dup_board[piece.pos] = piece.class.new(piece.color, piece.pos, dup_board)
    end
    dup_board
  end
  
  def set_white(row, col)
    if (row.even? && col.odd?) || (row.odd? && col.even?) 
      self[[row, col]] = Piece.new(:white, [row, col], self) #(color, pos, board, type)
    end
  end
  
  def set_red(row, col)
    if (row.even? && col.odd?) || (row.odd? && col.even?)
      self[[row, col]] = Piece.new(:red, [row, col], self)
    end
  end
  
  def set_pieces
    (0..7).each do |row|
      (0..7).each do |col|
        set_white(row, col) if row.between?(0, 2)
        set_red(row, col) if row.between?(5, 7)
      end
    end
  end
end
        
            
          