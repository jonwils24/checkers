require './board.rb'
require './piece.rb'

class Game
  attr_reader :board, :current_color
  
  def initialize
    @board = Board.new
    @current_color = :white
  end
  
  def play
    until @board.no_more_colors?(current_color)
      take_turn(@board, @current_color)
      @current_color = (@current_color == :white) ? :red : :white
    end
    
    puts board.render
    puts "#{current_color} lost"
    
    nil
  end
  
  def take_turn(board, current_color)
    puts @board.render
    puts "Current player: #{current_color}"
    
    from_pos = get_pos('From pos:')
    to_pos = get_pos('To pos:')
    @board[from_pos].perform_moves([to_pos])
    @board.pieces.each { |piece| piece.maybe_promote }
  end
  
  def get_pos(prompt)
    puts prompt
    input = gets.chomp.split(',').map { |move| Integer(move) }
  end
end