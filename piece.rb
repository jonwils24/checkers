require './board.rb'
require './game.rb'

class Piece
  attr_reader :color #:white on top, :red on bottom
  attr_accessor :type, :pos
  
  def initialize(color, pos, board, type = :non_king)
    @color = color
    @pos = pos
    @board = board
    @type = type
  end
  
  def inspect
    self.color == :white ? 'W' : 'R'
  end
  
  def neighbors
    neighbors = []
    self.all_slide_moves.each { |pos| neighbors << @board[pos] }
    neighbors.reject { |neighbor| neighbor.nil? }
  end
  
  def corner_opponents
    opponents = self.neighbors.select { |neighbor| neighbor.color != color }
    opponents.map { |opponent| opponent.pos }
  end
  
  def slide_offsets
    down_moves = [[1,1], [1,-1]]
    up_moves = [[-1, 1], [-1, -1]]
    
    if type == :king
      down_moves + up_moves
    else
      self.color == :white ? down_moves : up_moves
    end
  end
  
  def jump_offsets
    slide_offsets.map{ |offset| [offset.first * 2, offset.last * 2] }
  end
  
  def all_slide_moves
    slide_moves = []
    self.slide_offsets.each do |dx, dy|
      slide_position = [pos.first + dx, pos.last + dy]
      slide_moves << slide_position 
    end
    slide_moves
  end
  
  def possible_slide_moves
    all_slide_moves.select do |position|
      @board[position].nil? && @board.valid_pos?(position)
    end
  end
  
  def all_jump_moves
    jump_moves = []
    self.jump_offsets.each do |dx, dy|
      jumped_position = [pos.first + dx, pos.last + dy]
      jumped_over = [pos.first + (dx / 2), pos.last + (dy / 2)]
      if self.corner_opponents.include?(jumped_over)
        jump_moves << jumped_position 
      end
    end
    jump_moves
  end
  
  def possible_jump_moves
    all_jump_moves.select do |position| 
      @board[position].nil? && @board.valid_pos?(position)
    end
  end
  
  def perform_slide(end_pos)
    if self.possible_slide_moves.include?(end_pos)
      @board[self.pos], @board[end_pos] = nil, self
      self.pos = end_pos
      true
    else
      false
    end
  end
  
  def jumped_piece(end_pos)
    jumped_row = pos[0] - ((pos[0] - end_pos[0]) / 2)
    jumped_col = pos[1] - ((pos[1] - end_pos[1]) / 2)
    jumped = [jumped_row, jumped_col]
  end
  
  def perform_jump(end_pos)
    if self.possible_jump_moves.include?(end_pos)
      @board[jumped_piece(end_pos)] = nil
      @board[self.pos], @board[end_pos] = nil, self
      self.pos = end_pos
      true
    else
      false
    end
  end
  
  def maybe_promote
    promote_row = (self.color == :white) ? 7 : 0
    self.type = :king if self.pos[0] == promote_row
  end
  
  def perform_moves!(move_sequence)
    if move_sequence.length == 1
      if possible_jump_moves.include?(move_sequence.first)
        perform_jump(move_sequence.first)
      else
        perform_slide(move_sequence.first)
      end
    else
      move_sequence.each do |move|
        perform_jump(move)
      end
    end
  end
  
  def perform_moves(move_sequence)
    if valid_move_seq?(move_sequence)
      perform_moves!(move_sequence)
    else
      puts "InvalidMoveError"
    end
  end 
  
  def valid_move_seq?(move_sequence)
    dup_board = @board.dup
    dup_piece = dup_board[pos]
    begin
      dup_piece.perform_moves!(move_sequence)
    rescue
      false
    else
    true
    end
  end  
end
    
    
    
  
  
  
  